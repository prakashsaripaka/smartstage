package bst;



public class Node {
    
	private int data;
	
	Node left;
	Node right;
	
	/*public Node () {
		
	}*/
	
	
	public Node (int data) {
		this.data=data;
		this.left=null;
		this.right=null;
	}
	
	public int getdata() {
		return data;
	}
	public void setdata(int data) {
		this.data = data;
	}
	
	public Node getleft()
	{
		
		return left;
	}
	public Node getright()
	{
		
		return right;
	}
	
	public void setleft(Node left) {
		this.left = left;
	}
	public void setright(Node right) {
		this.right = right;
	}


      



}
