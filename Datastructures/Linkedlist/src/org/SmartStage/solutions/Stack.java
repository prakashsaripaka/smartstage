package org.SmartStage.solutions;

public class Stack {
	
	private StackNode head;
	private StackNode mid;
	//int count=0;
	//	private int size;
	
	public Stack()
	{	head = null;
//	mid =head;	
	//size = 0;
	}
	
	
	public Stack(int e)
	{
		
		StackNode n=new StackNode(e);
        head=n;
  //      mid=head;
        n.setNext(null);                  //////////optional ? check
	//   count++;
	}
	/*public Stack(SLLNode head)
	{	this.head = head;
		size = 1;
	}*/

	public StackNode getHead() {
		return head;
	}
	public StackNode getMid() {
		return mid;
	}
	
	public void setHead(StackNode head) {
		this.head = head;
	}
	/*public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}*/
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {

	if(head==null)
	{
		System.out.println("empty");
	}
	else
	{   StackNode temp=head;
		System.out.print(temp.getData()+" ");
		while(temp.getNext()!=null)
		{
			temp=temp.getNext();
			System.out.print(temp.getData()+" ");
		}
		
		
	}
	System.out.println("\n");
	System.out.println("$$$$$$$$$$$$$$$$$$$");
	}
	
	
	
	
	//#push
	public void push(int e){
		
		StackNode n=new StackNode(e);
		if(head==null)
	{
		
		
		head=n;
		head.setNext(null);
	   // count++;
	    //if(count%2==0)
	    	
	}
	else
	{
		StackNode temp=head;
		
		
		
	n.setNext(temp);
	head=n;
	
	}
	}
//#pop	
		public int pop(){
			
		//	StackNode n=new StackNode(e);
			StackNode temp=head;
			if(head==null)
		{
				
			System.out.println("empty");
			
		}
		else if(head.getNext()==null)
		
		{
		//	StackNode temp=head;
		head=null;
		}
		else
		{
		//	StackNode temp=head;
			
		head=head.getNext();	
			
				
		}
	
	return temp.getData();
		
		}
	
	//#isempty
		public boolean isempty()
		{ 
			if(head==null)
				return true;
			else
			return false;
		}
	
	//#peek
		
		public int peek()
		{ 
			if(head==null)
				return -1;
			else
			return head.getData();
		}
	
	
	
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		
		return true;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {

	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {

	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {

	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
}
