package array;

public class MinimumInRotated {

	
	
static int min(int arr[],int low,int high)
{
	//#using binary search in rotated array ...time complx=logn
		
			
		if(high<low)
			return arr[0];
		
		
		
		if(low==high)
			return arr[low];
	
		int mid=(low+high)/2;
		
		if(mid < high&&arr[mid+1] < arr[mid])
			return arr[mid+1];
		
		if(mid >low && arr[mid] < arr[mid-1])
			return arr[mid];
		
		
		
		if(arr[high]>arr[mid])
			return min(arr,0,mid-1);
		else
			return min(arr,mid+1,high);

}
	
	
	
	
	public static void main(String[] args) {
	

		int	arr[]={1,2,3,4,5};
		System.out.println(min(arr,0,arr.length-1));
	
	
	}

}
