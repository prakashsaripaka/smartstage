package org.SmartStage.solutions;

public class CLinkedList {
	
	private CLLNode head;
	//private int size;
	
	public CLinkedList()
	{	head=null;
		//head = head.getNext();
	//	size = 0;
	}
	
/*	public CLinkedList(CLLNode head)
	{	this.head = head;
	    
//		size = 1;
	}*/

	
	public CLinkedList(int e)
	{
		CLLNode n=new CLLNode(e);
		head=n;
		head.setNext(head);
		
		
		
	}
	
	public CLLNode getHead() {
		return head;
	}
	/*public void setHead(CLLNode head) {
		this.head = head;
	}*/
	/*public int getSize() {
		return size;
	}*/
	/*public void setSize(int size) {
		this.size = size;
	}*/
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {
		
		if(head==null)
		{
			System.out.println("Empty");
		}
		
		else
		{
			CLLNode temp=head;
			System.out.println(temp.getData()+" ");
		while(temp.getNext()!=head)
		{
			temp=temp.getNext();
			System.out.println(temp.getData()+" ");
			
		}
		
		}
		
		
		System.out.println("###################");
		
		
		
		
	
	
	}
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		if(head==null)
		return true;
		else
			return false;
		
	
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
           
	if(head==null)
	{
		CLLNode n=new CLLNode(e);
		head=n;
	    head.setNext(head); 	
	}
		
	else{
	CLLNode n=new CLLNode(e);
	CLLNode temp=head;
	n.setNext(head);
	while(temp.getNext()!=head)
	{
		temp=temp.getNext();
	}
	temp.setNext(n);
	head=n;
	
	}
	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {

		if(head==null)
		{
			CLLNode n=new CLLNode(e);
			head=n;
		    head.setNext(head); 	
		}
		else{
			CLLNode n=new CLLNode(e);
			CLLNode temp=head;
			n.setNext(head);
			while(temp.getNext()!=head)
			{
				temp=temp.getNext();
			}
			temp.setNext(n);
			
			//head=n;
			
			}
	
	
	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {

	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {

		return 0;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

	
		
		
		
		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {

	
	
		if(head==null)
		{
			System.out.println("empty"); 	
		}
		
		else if(head.getNext()==head)
		{
			head=null;
		}
		else{
			
			CLLNode temp=head;
			
			while(temp.getNext().getNext()!=head)
			{
				temp=temp.getNext();
			}
			temp.setNext(head);
			
			//head=n;
			
			}
	
	
	
	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
}
