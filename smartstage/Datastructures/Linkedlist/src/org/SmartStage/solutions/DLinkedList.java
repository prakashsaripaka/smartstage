package org.SmartStage.solutions;

public class DLinkedList {
	
	private DLLNode head;
	//private int size;
	
	public DLinkedList()
	{	head = null;
		//next=null;
		
	//size = 0;
	}
	
	public DLinkedList(int e)
	{	
		DLLNode n=new DLLNode(e);
		head = n;
	          
	
	    n.setNext(null);
	    n.setPrev(null);
	     
	    
	     
	
	//size = 1;
	}

	public DLLNode getHead() {
		return head;
	}
	public void setHead(DLLNode head) {
		this.head = head;
	}
	/*public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}*/
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {

	
	if(head==null)
		System.out.println("empty");
	else
	{     DLLNode temp=head;
		System.out.println(head.getData()+" ");
		while(temp.getNext()!=null)
		{
			temp=temp.getNext();
			System.out.println(temp.getData()+" ");
		}
	}
	System.out.println("\n%%%%%%%%%%%%%%%%%%% ");
	
	}
	
	
	//#1. Returns the number of elements in the list.
	public int size() {
		
		return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		if(head==null)
		  return true;
		else
			return false;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
        DLLNode n=new DLLNode(e);
	if(head==null)
	{
		head=n;
		n.setNext(null);
		n.setPrev(null);
		
	}
	else
	{
		
		DLLNode temp=head;
		n.setNext(temp);
		n.setPrev(null);
		temp.setPrev(n);
		head=n;
	}
	
	
	
	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {

		   DLLNode n=new DLLNode(e);
			if(head==null)
			{
				head=n;
				n.setNext(null);
				n.setPrev(null);
				
			}
	
			else
			{
				DLLNode temp=head;
				while(temp.getNext()!=null)
				{
					temp=temp.getNext();
					
					
				}
				n.setPrev(temp);
				temp.setNext(n);
				n.setNext(null);
				
				
			}
	
	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {

	
		 DLLNode n=new DLLNode(e);
		int count=1;	
		 if(head==null)
			{
				head=n;
				n.setNext(null);
				n.setPrev(null);
				
			}
	
		 else if(i==1)
		 {
			 addFirst(e);
		 }
		 
		 else{
				DLLNode temp=head;
				System.out.println(temp.getData()+"first");
				while(count<i && temp.getNext()!=null )
				{
					temp=temp.getNext();
					count++;
				}
				
			n.setNext(temp);
			n.setPrev(temp.getPrev());
			temp.getPrev().setNext(n);
			temp.setPrev(n);
			
			
			
			}
	
	
	}
	
	
	
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {
           int  index =-1;
		 int c=1;
           if(head==null)
		{
			System.out.println("empty");
			
		
		}
		else{
		DLLNode temp=head;
			while(temp.getData()!=e && temp.getNext()!=null )
			{
				c++;
				temp=temp.getNext();
			}  
          if(temp.getData()==e)
          {index=c;}
		 
		
		}
		
		
		
		return index;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {

		return 0;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {

		return false;
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		return 0;
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() {

		return 0;
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		return 0;
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {

	}
	
	//#13. Removes the first element in the list.
	public void removeFirst() {

	}
	
	//#14. Removes the last element in the list.
	public void removeLast() {

	}
	
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence() {

	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence() {

	}
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

	}
	
}
