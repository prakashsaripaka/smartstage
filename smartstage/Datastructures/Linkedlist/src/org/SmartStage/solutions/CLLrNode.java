package org.SmartStage.solutions;

//SinglyLinkeListNode
public class CLLrNode {

	private int data;
	private CLLrNode next;
	
	public CLLrNode (int data) {
		this.data=data;
		this.next=null;
	}
	
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public CLLrNode getNext() {
		return next;
	}
	public void setNext(CLLrNode next) {
		this.next = next;
	}
		
}
