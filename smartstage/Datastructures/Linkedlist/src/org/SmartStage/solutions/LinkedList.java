package org.SmartStage.solutions;

public class LinkedList {
	
	private SLLNode head;
	//private int size;
	
	public LinkedList(int arr[])
	{
		
		for(int i=(arr.length-1);i>=0; i--)
		{
			addFirst(arr[i]);
			
		}
	}
	
	public LinkedList(LinkedList list1)
	{
		                                  //copy into this linkedlist the passed linkedlist
	    head=list1.head;
		
	}
	
	
	public LinkedList()
	{	head = null;
		//size = 0;
	}
	
	public LinkedList(SLLNode head)
	{	this.head = head;
		//size = 1;
	}

	public SLLNode getHead() {
		return head;
	}
	public void setHead(SLLNode head) { //replace with int
		this.head = head;
	}
	//public int getSize() {
		//return size;
	///}
	//public void setSize(int size) {
		//this.size = size;
	//}
	
	//Write the implementation for the following methods.
	
	
	//#0. Prints all elements in the list.
	public void traverse() {

		SLLNode current = head;
		 
		if(current==null)
		 {
			 
			System.out.print("empty");
			 
		 }
		 else
		 {
			 System.out.println(current.getData());
			 while(current.getNext()!=null)
			 {
				 current=current.getNext();
				 System.out.println(current.getData());
				 			 }
		   	 
		  
		  // current.setNext(head);          ////for checking loop
	 	 } 

	
	
	/*	SLLNode current = head;
		while(current.getNext()!=null)
		{
			System.out.println(current.getData());
		      current=current.getNext();	
		}*/
	//	
	
	}
	
	//#1. Returns the number of elements in the list.
	public int size() {
		SLLNode temp = head;
		 int s=1;
	//	 if(temp==null)
		// {
			 
			//return s;
			 
		// }
		// else
		 //{
			 
		if(temp==null)
		{
			return -1;
		}
		else
		{
		while(temp.getNext()!=null)
			 {
				 temp=temp.getNext();
				 s++;		 			 
		 }
		 
		return s; 
		} 
	

	
	//	return 0;
	}
	
	//#2. Returns true if the list contains zero elements, else return false.
	public boolean isEmpty() {
		
		if(size()==0)
		
		return true;
		
		else
			return false;
	}
	
	//#3. Adds the element 'e' to the beginning of the list.
	public void addFirst(int e) {
            
	 SLLNode n = new SLLNode(e);	
	 
//	 if(head==null)
//	 {
//		 
//		head=n; 
//	 }
	// else
	 //{
		 
		 n.setNext(head);
		 head=n;
	 //}
	
	
	}
	
	//#4. Adds the element 'e' to the end of the list.
	public void addLast(int e) {
 
		SLLNode temp = head;
 
 if(temp==null)
 {
	 
	 head = new SLLNode(e);
	 
 }
 else
 {
	 while(temp.getNext()!=null)
	 {
		 
		 temp=temp.getNext();
	 }
   	 
  temp.setNext(new SLLNode(e));
 }
	
	}
	
	//#5. Adds the element 'e' at the ith position in the list.
	public void add(int e, int i) {

		SLLNode temp = head;
		SLLNode m = new SLLNode(e); 	
		
		if(i==1)
			addFirst(e);
		else
		{
		for (int k=0;k<i-2;k++)
		{
		   temp=temp.getNext();
		   
		   }
	     m=temp.getNext();
	    SLLNode n = new SLLNode(e);
	    temp.setNext(n);
	    n.setNext(m);
		}
	}
	
	//#6. Returns the index for first occurrence of element 'e' in the list.
	public int firstIndexOf(int e) {
     SLLNode temp = head;
       int count=0;
       
       while(temp!=null && temp.getData()!=e)
       {
    	   temp=temp.getNext();
           count++;
       }
    	if(temp==null)
    		return -1;
    	else
       return count;
	}
	
	//#6. Returns the index for last occurrence of element 'e' in the list.
	public int lastIndexOf(int e) {
		SLLNode temp = head;
		int index=-1,count=0;
		while(temp!=null)
	       {
	    	   if( temp.getData()==e)
	    	   {
	    		   index=count;
	    	   }
			temp=temp.getNext();
	           
	    	   
	    	   count++;
	       }
		
		return index;
	}
		
	//#8. Returns true if the element 'e' is present in the list, else return false.
	public boolean contains(int e) {
          
		SLLNode temp = head;
		int check=0;
		while(temp!=null)
	       {
	    	   if( temp.getData()==e)
	    	   {
                  check++;	    		   
                 break;	    	   
	    	   }
	    	   
	    	   temp=temp.getNext();

	       }
		
		if(check==0)
			return false;
		else
			return true;
		
	}
	
	//#9. Returns the element which occurs at ith position in the list (but doesn't remove it).
	public int get(int i) {

		SLLNode temp = head;
	//	SLLNode m = new SLLNode(e); 	
		
		for (int k=0;k<i-1;k++)
		{
		   temp=temp.getNext();
		   
		   }
        		
		
		
		return temp.getData();
	}
	
	//#10. Returns the first element in the list (but doesn't remove it).
	public int getFirst() throws Exception {
              if(head!=null) 
		return head.getData();
              else
            	  throw new Exception();
	
	}
	
	//#11. Returns the last element in the list (but doesn't remove it).
	public int getLast() {

		SLLNode temp = head;
		//int check=0;
		while(temp.getNext()!=null)
	       {
	    	temp=temp.getNext();
	       }
			//if( temp.getData()==e)
	    	
			//{
                //  check++;	    		   
               //  break;	    	   
	    	   //}
	    	
		
		
		return temp.getData();
	}
	
	//#12. Removes the element which occurs at ith position in the list.
	public void remove(int i) {
             
            	 
	      
		SLLNode temp = head;
		
		
		//SLLNode m = new SLLNode(e); 	
		SLLNode m;
		
		 if(i==1)
			{
			removeFirst();
			
		
			}
		else
		{                           ///!!!!how to remove element at position 1
		
			
			for (int k=0;k<i-2;k++)
		{
		   temp=temp.getNext();
		   
		   }
	
	m=temp.getNext();
	temp.setNext(m.getNext());
		}
		}	
	
	
	//#13. Removes the first element in the list.
	public void removeFirst() {
		
		SLLNode temp = head;
head=temp.getNext();
	
	
	}
	
	
	//#14. Removes the last element in the list.
	public void removeLast() {
        //head null exception
		//
		//if(head==null)
		
		SLLNode temp=head;
	   // SLLNode prev;    
		//if(head.getNext()==null)
		//	head.setNext(null);
	//	else
		//{
		if(temp.getNext()==null)
		{
			head=null;
		}
		else{
		while((temp.getNext()).getNext()!=null)         ///!!!if thr is only one element in the list
			                                                ////size factor
	         {
	        	temp=temp.getNext();
	        //if((temp.getNext()).getNext()==null)	
	                       
	        }
	
         temp.setNext(null);   	
	
		}
		}
	//}
	//#15. Removes the first occurrence of element 'e' in the list.
	public void removeFirstOccurrence(int e) {

	
		 SLLNode temp = head;
	     //  int count=0;
		 SLLNode m;
	       
		 
	//	 while(temp.getData()!=e)
	  //     {
		if(temp.getData()==e)
		{
			head=temp.getNext();
			
		}
		 
		else{
		 
		 while(temp.getNext()!=null)
		       {
	    	   if((temp.getNext()).getData()==e )
				 break;
				 temp=temp.getNext();
	   //        count++;
	       }
	    	//temp=temp.getNext();
	       //if(temp==null)
	    	//	return -1;
	    	//else
	       //return count;
	       m=temp.getNext();
	   	temp.setNext(m.getNext());
		}
	}
	
	//#16. Removes the last occurrence of element 'e' in the list.
	public void removeLastOccurrence(int e) {

		SLLNode temp = head;
	     //  int count=0;
		 SLLNode m;
		 SLLNode k=new SLLNode(0);
		if(temp.getNext()==null&&temp.getData()==e)
		{
			head=null;
		}
			else
		 {
		 
		 while(temp.getNext()!=null)
	       {
  	   if((temp.getNext()).getData()== e )
  	               {k = temp ;
  	               }
			 temp=temp.getNext();
 //        count++;
     }
  	//temp=temp.getNext();
     //if(temp==null)
  	//	return -1;
  	//else
     //return count;
     m=k.getNext();
 	k.setNext(m.getNext());
		 }
}
	
	
	
	
	
	
	
	//#17. Removes all the elements in the list.
	public void removeAll() {

	  head=null;
	}
	
	//#18. Replaces the element at ith position in the list with element 'e'.
	public void set(int e, int i) {

		//if(head==null)
			
		
		
		SLLNode temp = head;
		//SLLNode m = new SLLNode(e); 	
		SLLNode m;
		if(i==1)
		{
			removeFirst();
		}
		
		
		for (int k=0;k<i-2;k++)
		{
		   temp=temp.getNext();
		   
		   }
		SLLNode n = new SLLNode(e);
	m=temp.getNext();
	temp.setNext(n);
	
	n.setNext(m.getNext());     
	
	}
	
//#19reverse a linkedlist
	
	public void reverse(){
		
		SLLNode temp=head;
		SLLNode prev=null,next;
		if(head==null)
		{
		System.out.println("empty");	
		}
		else
		{
		while(temp!=null)
		{
			next=temp.getNext();
			temp.setNext(prev);
            prev=temp;
            temp=next;
			
			
		}
		
	head=prev;
	}

	}


//#20reverse the node of linked list in a pair on two elements.
	public void reversepairoftwo(){
		if(head==null||head.getNext()==null)
		{
			
			System.out.println("no change");
		}
		else{
			
			SLLNode p1=head,p2,n;
			head=p1.getNext();
			while(p1!=null&&p1.getNext()!=null)
			{
				p2=p1.getNext();
				n=p2.getNext();
				p2.setNext(p1);
				if(n.getNext()==null)
					p1.setNext(n);
				else
				p1.setNext(n.getNext());
				p1=n;
				
			}
			
			
		}
		
	}



//#21 Find kth last element of a linked list.
	
	public int kthfromlast(int i){
	
	if(head==null)
	{
		System.out.println("empty");
	    return -1;  
	}
	else 
	{
	SLLNode	temp1=head,temp2=head;
	int count=1,k=0;
	
	while(temp1.getNext()!=null)
	{
		count++;
		temp1=temp1.getNext();
		
	}
		
	while(k<count-i)
	{
		temp2=temp2.getNext();
	
	k++;
	}
	
	return temp2.getData();
	}
	}

 //#22swap two nodes of a linked list.
	
	public void swap(int m ,int n)
	{
		
		SLLNode p1 = head,p2=head,temp;
	       
		//int count=0;
	    if(p1.getData()==m)
	    {
	    	
	    	while(p2!=null && p2.getNext().getData()!=n)
		       {
		    	   p2=p2.getNext();
		           //count++;
		       }
	    	
	    	head=p2.getNext();
	    temp=head.getNext();
	    head.setNext(p1.getNext());
	    p2.setNext(p1);
	    p1.setNext(temp);
	    
	    }
	       
	   
	   
	    	
	    	
	   
	    	
	    
	    else
	    {
		while(p1!=null && p1.getNext().getData()!=m)
	       {
	    	   p1=p1.getNext();
	           //count++;
	       }
	    	
	       while(p2!=null && p2.getNext().getData()!=n)
	       {
	    	   p2=p2.getNext();
	           //count++;
	       }
		temp=p1.getNext();
		p1.setNext(p2.getNext());
		p2.setNext(temp);
		temp=p1.getNext().getNext();
		p1.getNext().setNext(p2.getNext().getNext());
		p2.getNext().setNext(temp);
		
		
	    }
	
	
	}
	
	//#23 detecting a loop
	
	public boolean detectloop()
	{
		
	if(head==null)
	{
		System.out.println("empty");
	return false;
	}
		
	else
	{
		
		SLLNode a=head;
		SLLNode b=a.getNext();
	
	//while(a.getData()!=b.getData()||b.getNext().getNext()==null||a.getNext()==null   )
	while(b!=null)
		{
		a=a.getNext();
		if(b.getNext()==null)
			return false;
		
		
		b=b.getNext().getNext();
		if(b==null)
			return false;
					
		
		
			else if	(a.getData()==b.getData())
		{return true;}
		
		
		}
	
		
		return false;
	
	
	}
	
	
	
	}
	
	
	
	
	//#24 Merging two linked list such that elements from the both the list are alternate.

public void mergetwolist(LinkedList a,LinkedList b)

{
	if(a.head==null)
		a.head=b.head;
	else if(b.head==null)
	{
		
	}

	else
	{
	SLLNode c1=a.head,temp1,temp2;
	SLLNode c2=b.head;
//c1.head.setNext(c2.head);

	
	while(c1.getNext()!=null && c2.getNext()!=null)
		
	{
		temp1=c1.getNext();
	
		c1.setNext(c2);
		temp2=c2.getNext();
		c2.setNext(temp1);
		c2=temp2;
		c1=temp1;
		
		
	}
	}





}



//# reverse last kth elements of a linked list
       public void reverselastk(int i)
       {
              
    	  // SLLNode temp=head;
    	   
    	   if(head==null)
    		{
    			System.out.println("empty");
    		    //return -1;  
    		}
    		else 
    		{
    		SLLNode	temp1=head,temp2=head,n,nn=null,temp2prev;
    		int count=1,k=0;
    		
    		while(temp1.getNext()!=null)
    		{
    			count++;
    			temp1=temp1.getNext();
    			
    		}
    			
    		while(k<count-i-1)
    		{
    			temp2=temp2.getNext();
    		
    		k++;
    		}   
         
    		temp2prev=temp2;
    		temp2=temp2.getNext();
    		/*  temp2firstnext=temp2.getNext();
           temp2copy=temp2; 
           while(temp2.getNext()!=null)
            {
        	
             n=temp2.getNext();
             nn=n.getNext();
             if(temp2.getNext().getNext()==null)
             {
            	 nn.setNext(n);
             
             break;
             }         
             else
             {
            t=nn.getNext();
            nn.setNext(n);
            temp2=n;
             }
            }
       
*/             /*temp2firstnext.setNext(null);
    		temp2copy.setNext(nn);*/
    		
    		///SLLNode temp=head;
    		SLLNode prev=null,next;
    		//if(head==null)
    	//	{
    	//	System.out.println("empty");	
    	//	}
    	//	else
    	//	{
    		while(temp2!=null)
    		{
    			next=temp2.getNext();
    			temp2.setNext(prev);
                prev=temp2;
                temp2=next;
    			
    			
    		}
    		
    	temp2prev.setNext(temp2);
    		
    		
    		
    		}
       
       
       }
//# swap the list of first k elements with the last k elements in a single linked list.
     public void swapfistkwithlastk(int k)
     {
    	SLLNode temp1=head,temp2=head;
    	int i=1;
    	if(head==null||temp1.getNext()==null)
    		System.out.println("NOT enough elements");
    	else if(temp1.getNext().getNext()==null)
    	{
    	temp2=temp1.getNext();
    	temp2.setNext(temp1);
    	temp1.setNext(null);
    	head=temp2;
    	
    	}
    	else
    	{
    	while(i<k){
    		temp1=temp1.getNext();
    	     i++;
    	          }
       	 
             
    	SLLNode	temp=head;
    	int count=1,j=0;
    	
    	while(temp.getNext()!=null)
    	{
    		count++;                                //temp-last element
    		                                       //temp1-3
    		                                    //temp2=5
    		                                       //head=1
    		temp=temp.getNext();
    		
    	}
    		
    	while(j<count-k-1)
    	{
    		temp2=temp2.getNext();
    	
    	j++;
    	}
    	SLLNode kkk=temp2.getNext();                     //kkk=6
    	temp.setNext(temp1.getNext());
    	temp2.setNext(head);
    	head=kkk;
    	temp1.setNext(null);
     
    	}
     }

     // #reverse the last kth element
             public void reverselastkth(int k)                               // 1,2,3,4,5,6
             {                                                                // 1,2,3,6,5,4
            	  SLLNode prev=null,next,kkk;                            //prev=null
            	                                               //temp=4
            	  SLLNode	temp2=head,temp=head;              // next=5
              	int count=1,j=0;
              	
              	while(temp2.getNext()!=null)                            //temp2==3(fix)
              	{
              		count++;                                
              		                                                   		                                    //temp2=5
              		                                      
              		temp2=temp2.getNext();
              		
              	}
              		
              	while(j<count-k-1)
              	{
              		temp=temp.getNext();
              	
              	j++;
              	}    	                                                //next=5
              //  System.out.println(temp.getData());
              	kkk=temp;
              //	System.out.println(temp2.getData());
              	temp=temp.getNext();
             // 	System.out.println(temp.getData());
                   while(temp!=null)
                   {
                	
                	next=temp.getNext();
                	//System.out.println("next"+next.getData());
                	temp.setNext(prev);
                	//System.out.println("temp"+temp.getData());
                	prev=temp;
                	//System.out.println("prev"+prev.getData());
                	temp=next;
                	   }
                  // System.out.println(temp.getData()); 
                   kkk.setNext(temp2);
                        
             }


//#delete N nodes after M nodes of a linked list
             
             public void deleteNafterM(int n,int m){
             
             SLLNode temp=head,curr=head;
             if(temp==null)                                 //1,2,3,4,5,6,7
            	 System.out.println("EmPTY");
             else
             {
            	 while(temp!=null)
            	 {
            	 for(int i=1;i<m && temp!=null;i++)
            	 {
            		 
            		 temp=temp.getNext();
            		 
            	 }
            	 if(temp==null)
            		 return;
            	 
            	 
            	 curr=temp;
            	 
            	 for(int j=0;j<n;j++)
            	 {
            		 temp=temp.getNext();
            	        if(temp.getNext()==null)
            	        {
            	        	curr.setNext(null);
            	        	return;
            	        }
            	 
            	 }             
                 temp=temp.getNext();
            	 curr.setNext(temp);
                 
             
             
                   }
             
             
               }
          }
  



}



