package bst;

public class Bst {

private Node root;

 public Node getRoot() 
 {
   return root;	
  }

 public  Bst() 
 {
    
	  root = null;	
  }    

 
 
 public  Bst(int d) 
 {
    
	  root = new Node(d);	
  }    
	      
	
 public void InOrderTraversal(Node n){
	 
	if(n!=null)
	{
		InOrderTraversal(n.getLeft());
		System.out.print(" "+n.getData());
		InOrderTraversal(n.getRight());
		
	}
 }
	public void PreOrderTraversal(Node n){
		
		if(n!=null)
		{
			System.out.print(" "+n.getData());
			PreOrderTraversal(n.getLeft());
			
			PreOrderTraversal(n.getRight());
			
		}

 }
	
public void PostOrderTraversal(Node n){
		
		
	if(n!=null)
		{
			
			PostOrderTraversal(n.getLeft());
			
			PostOrderTraversal(n.getRight());
			System.out.print(" "+n.getData());
		}

 }

public void insert(int i){
	
  Node	newnode = new Node(i);
	
  if(root==null)
	  root=newnode;
      
  else{
Node  temp=root;
	while(true)
	{
		
		if(temp.getData()>=newnode.getData())
		{
			
			if(temp.getLeft()!=null)
				temp=temp.getLeft();
			else
			{
				temp.setLeft(newnode);
				break;
			}
			
		}
	
		else
		{
			if(temp.getRight()!=null)
				temp=temp.getRight();
			else
			{
				temp.setRight(newnode);
				break;
			}
			
		}
	
	
	}
  
  
  
  }
}

public boolean Search(int k){
	return Search(root,k);
	
}


public boolean Search(Node n,int k){
	Node temp=n;

	if(temp==null)
    {
      
     return false;
    }
else
{
    if(temp.getData()==k)
    	return true;
    if (temp.getData()>=k)
    	return Search(temp.getLeft(),k);
    
    else
    	return Search(temp.getRight(),k);

}

}


public boolean Delete(int k){
	return Search(root,k);
	
}
public boolean Delete(Node n,int k)
{        
	     Node temp=n,parent=n;
    	boolean isItALeftChild=true;
        while(temp.getData()!=k){
         parent =temp;  //parent of required node
        	if(k<temp.getData())
        	{
        		isItALeftChild=true;
        		temp=temp.getLeft();
        	}
        	else{
        		isItALeftChild=false;
        		temp=temp.getRight();
        		
        	}
        
        if(temp==null)
        	return false;
        
        }
	if(temp.getLeft()==null&&temp.getRight()==null){
		 if(temp==root)
		     root=null;
		 else if(isItALeftChild){
			 parent.setLeft(null);
			 
		      }
		 else{
			 parent.setRight(null);
		 }
	
	}
	else if(temp.getRight()==null){
		if(temp==root)
			root=temp.getLeft();
		else if(isItALeftChild)
			parent.setLeft(temp.getLeft());
		else
			parent.setRight(temp.getLeft());
		
	} 	

	else if(temp.getLeft()==null){
		if(temp==root)
			root=temp.getRight();
		else if(isItALeftChild)
			parent.setLeft(temp.getRight());
		else
			parent.setRight(temp.getRight());
		
	} 	

	else{
		
		Node replacement =getReplacementNode(temp);
		 if(temp==root)
			 root=replacement;
		 else if(isItALeftChild)
			 parent.setLeft(replacement);
		 else
			 parent.setRight(replacement);
		 
		 replacement.setLeft(temp.getLeft());
	}

       return true;

}

public Node getReplacementNode(Node replacedNode){
	
	Node replacementParent=replacedNode;
	Node replacement = replacedNode;
	
  Node temp=replacedNode.getRight();
              
  while(temp!=null)
  { replacementParent=replacement;
	  replacement =temp;
     temp=temp.getLeft();}

   if(replacement!=replacedNode.getRight())
   {
	   replacementParent.setLeft(replacement.getRight());
	   replacement.setRight(replacedNode.getRight());
   }

return replacement;
} 




}



