def add(i,j):
    return i+j

def double(i):
    return i*2

def half(i):
    return i//2

def mult(n,m):
    if n==1:
        return m
    elif n%2!=0 :
        return m + mult(half(n),double(m))
    else:
        return mult(half(n),double(m))


n=int(input("Enter 1st number: "))
m=int(input("Enter 2nd number: "))
print(mult(n,m))
