__author__ = 'Prakash Saripaka'

def findmax(list):
    if(len(list)==1):
        return list[0]
    else:
        mid=len(list)//2
        leftlist =[list[i] for i in range(0,mid)]
        rigthlist =[list[i]for i in range(mid,len(list))]
        a = findmax(leftlist)
        b = findmax(rigthlist)
        return max(a,b)

list=[1,2,3]
print(findmax(list))